# 21dthd4


## Họ tên: Nguyễn Nhân Quỳnh Như
## MSSV: 2180608719
## Lớp: 21DTHD4

| Title               | Teacher changes students's score               |
|:-------------------:|:-----------------------------------------------|
| Value Statement     | As a teacher, I want to change student's score | 
| Acceptence Criter   | Acceptence Criterion 1: If student doesn't take the exam, they get 0 points.<br>Acceptence Criterion 2: If their score is wrong then correct it.|
| Definition Of Done  | - Unit Tests Passed<br>- Acceptence Criteria Met<br>- Code Reviewed<br>- Functional Tests Passed<br>- Non-Functional Requirements Met<br>- Product Owner Accepts User Story|
| Owner               | Nguyen Nhan Quynh Nhu                          | 
##
![Semantic description of image](UI.png)
##
## Họ tên: Nguyễn Hoàng Chương
## MSSV: 2180607334
## Lớp: 21DTHD4
| Title                | Teacher changes students's score               |
|:--------------------:|:-----------------------------------------------|
| Value Statement      | As a Manager,<br>I want to arrange student with increasing avgScore<br>so I can easily find the student with the highest avgScore.             |
| Acceptance Criterion | Acceptance Criterion 1:<br>When managing chooses to select the sorting option,<br>the student avgScore will be sorted ascending or descending. |
| Definition Of Done   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner                | Nguyen Hoang Chuong                            | 
[Thêm hình ảnh]
![Semantic description of image](https://i.imgur.com/J3cwoVt.png)



##
## Họ tên: Huynh My Nhat Mai
## MSSV: 2180607724
## Lớp: 21DTHD4

| Title                | Search students's information base on ID       |
|:--------------------:|:-----------------------------------------------|
| Value Statement      | As a Manager,<br>I want to find student easily.|
| Acceptance Criterion | Acceptance Criterion 1:<br>Check if that student exist<br>Find students to checkout their score |
| Definition Of Done   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner                | Huynh My Nhat Mai                           | 

##
## Họ tên: Lương Thiện Hưng
## MSSV: 2180607587
## Lớp: 21DTHD4
| Title                |  Rank students based on average score      |
|:--------------------:|:-----------------------------------------------|
| Value Statement      | As a teacher,I want to know who is the best student.|
| Acceptance Criterion | Acceptence Criterion 1: Displays the average score of each student<br>Acceptence Criterion 2: Displays the rank and title of each student   |
| Definition Of Done   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner                | Luong Thien Hung                                   | 
<<<<<<< README.md
##
![Semantic description of image](387537968_1529999864403502_4538188284849181568_n.png) 
##
=======

##
## Họ tên: Đặng Văn Đạt
## MSSV: 2180607411
## Lớp: 21DTHD4
| Title                | Teacher insert student to the list             |
|:--------------------:|:-----------------------------------------------|
| Value Statement      | If there is new,as a teacher,I want to add them in list.|
| Acceptance Criterion | Acceptance Criterion:Add new student in list|
| Definition Of Done   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner                | Dang Van Dat                                   | 


